<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Assert;

//Request::setTrustedProxies(array('127.0.0.1'));

$app->match('/', function (Request $request) use ($app) {
    //auth user
    if (null === $user_id = $app['session']->get('user_id')) {
        $user_id = uniqid();
        $app['session']->set('user_id', $user_id);
    }

    $userDir = __DIR__ . '/../var/files/' . $user_id;
    $workingFile = $userDir . '/working';
    $sitemapFile = $userDir . '/sitemap.xml';

    if (file_exists($userDir) && file_exists($workingFile)) {
        return $app->redirect('/view');
    }

    /** @var \Symfony\Component\Form\Form $parseForm */
    $parseForm = $app['form.factory']->createNamedBuilder('parseForm', 'form')
        ->add('url', 'url', array(
            'required' => true,
            'label' => false,
            'attr' => [
                'placeholder' => 'Введіть валідний url',
                'class' => 'input-text'
            ],
            'constraints' => array(new Assert\NotBlank(), new Assert\Url())
        ))->add('save', 'submit', array(
            'label' => 'Go!',
            'attr' => [
                'class' => 'btn btn-primary btn-lg btn-block'
            ],
        ))
        ->getForm();

    $parseForm->handleRequest($request);

    if ($parseForm->isValid()) {
        $data = $parseForm->getData();

        $workingFile = $userDir . '/working';
        if (!file_exists($userDir)) {
            mkdir($userDir);
        }
        file_put_contents($workingFile, json_encode(['all' => 1, 'queue' => 1, 'parsed' => 0]));

        exec('php ' . __DIR__ . '/../bin/console parse ' . $user_id . ' ' . $data['url'] . ' > ' . $userDir . '/parsing.log &');

        return $app->redirect('/view');
    }

    return $app['twig']->render('index.html.twig', array(
        'user_id' => $user_id,
        'form' => $parseForm->createView(),
        'generated' => file_exists($sitemapFile),
    ));
})
    ->bind('homepage');

$app->get('/view', function () use ($app) {
    if (null === $user_id = $app['session']->get('user_id')) {
        $user_id = uniqid();
        $app['session']->set('user_id', $user_id);
    }

    $userDir = __DIR__ . '/../var/files/' . $user_id;
    $workingFile = $userDir . '/working';
    $sitemapFile = $userDir . '/sitemap.xml';

    if (!file_exists($sitemapFile) && !file_exists($workingFile)) {
        return $app->redirect($app['url_generator']->generate('homepage'));
    }

    $wdata = null;
    if (file_exists($workingFile)) {
        $wdata = json_decode(file_get_contents($workingFile));
    }

    if ($wdata) {
        return $app['twig']->render('view.html.twig', array(
            'wdata' => $wdata,
        ));
    } else {
        return $app
            ->sendFile($sitemapFile)
            ->setContentDisposition(ResponseHeaderBag::DISPOSITION_INLINE);
    }

});

$app->get('/log', function () use ($app) {
    if (null === $user_id = $app['session']->get('user_id')) {
        $user_id = uniqid();
        $app['session']->set('user_id', $user_id);
    }

    $userDir = __DIR__ . '/../var/files/' . $user_id;
    $workingFile = $userDir . '/working';
    $logFile = $userDir . '/parsing.log';

    if (!file_exists($logFile)) {
        return $app->redirect($app['url_generator']->generate('homepage'));
    }

    return $app
        ->sendFile($logFile)
        ->setContentDisposition(ResponseHeaderBag::DISPOSITION_INLINE);
});

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/' . $code . '.html.twig',
        'errors/' . substr($code, 0, 2) . 'x.html.twig',
        'errors/' . substr($code, 0, 1) . 'xx.html.twig',
        'errors/default.html.twig.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});
