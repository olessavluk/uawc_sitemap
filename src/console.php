<?php

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\CssSelector;
use GuzzleHttp\Client;

CONST MAX_PARSE_COUNT = 200;
CONST MAX_DEPTH = 2;

function effectiveUrl($url)
{
    $query = parse_url($url, PHP_URL_QUERY);
    return parse_url($url, PHP_URL_SCHEME) . '://' .
    parse_url($url, PHP_URL_HOST) . parse_url($url, PHP_URL_PATH) .
    ($query ? '?' . $query : '');

}

$console = new Application('My Silex Application', 'n/a');
$console->getDefinition()->addOption(new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The Environment name.', 'dev'));
$console->setDispatcher($app['dispatcher']);
$console
    ->register('parser')
    ->setDefinition(array(
        new InputArgument('uid', InputArgument::REQUIRED, 'Unique identifier'),
        new InputArgument('url', InputArgument::REQUIRED, 'Site url'),
    ))
    ->setDescription('Parses specified url')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
        $uid = $input->getArgument('uid');
        $url = $input->getArgument('url');

        $commonDir = __DIR__ . '/../var/files';
        $userDir = $commonDir . '/' . $uid;
        $workingFile = $userDir . '/working';
        $sitemapFile = $userDir . '/sitemap.xml';

        if (!file_exists($userDir)) {
            mkdir($userDir);
        }
        file_put_contents($workingFile, json_encode(['all' => 1, 'queue' => 1, 'parsed' => 0]));

        $base_url = parse_url($url, PHP_URL_SCHEME) . '://' . parse_url($url, PHP_URL_HOST);
        $client = new Client([
            'base_url' => $base_url,
        ]);

        $links[effectiveUrl($url)] = 0;
        $queue[] = effectiveUrl($url);

        for ($i = 0; $i < count($queue) && $i < MAX_PARSE_COUNT; $i++) {
            $url = $queue[$i];
            $depth = $links[$queue[$i]];

            $output->writeln($i . ' - ' . $url);
            file_put_contents($workingFile, json_encode(['all' => count($links), 'queue' => count($queue) - $i, 'parsed' => $i]));

            try {
                $response = $client->get($url);
            } catch (Exception $e) {
                $output->writeln($e->getMessage());
                continue;
            }

            $crawler = new Crawler((string)$response->getBody(true), $url, $base_url);
            $onPageLinks = $crawler->filter('a')->links();

            foreach ($onPageLinks as $link) {
                $url = effectiveUrl($link->getUri());
                if ( strpos(parse_url($url, PHP_URL_SCHEME), 'http') !== false
                    && parse_url($url, PHP_URL_HOST) == parse_url($base_url, PHP_URL_HOST)
                    && !array_key_exists($url, $links))
                {
                    $links[$url] = $depth + 1;

                    $output->writeln(' -> ' . $url);

                    if ($depth < MAX_DEPTH) {
                        $queue[] = $url;
                    }
                }
            }
        }
        $links = array_keys($links);

        $sitemapContent = $app['twig']->render('sitemap.xml.twig', array(
            'urls' => $links,
        ));

        file_put_contents($sitemapFile, $sitemapContent);

        unlink($workingFile);
    });

return $console;