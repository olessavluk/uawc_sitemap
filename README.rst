Running Application
-----------------------------

To see project in action, start the PHP built-in web server with
command:

.. code-block:: console

    $ cd path/to/install
    $ composer install
    $ bower install
    $ composer run

Then, browse to http://localhost:8888/
or you can go to http://sitemap.olessavluk.me/